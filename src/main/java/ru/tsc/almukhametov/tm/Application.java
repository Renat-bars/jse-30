package ru.tsc.almukhametov.tm;

import ru.tsc.almukhametov.tm.component.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}