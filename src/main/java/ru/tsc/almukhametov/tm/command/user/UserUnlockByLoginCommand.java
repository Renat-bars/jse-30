package ru.tsc.almukhametov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public class UserUnlockByLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.USER_UNLOCK_BY_LOGIN;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.USER_UNLOCK_BY_LOGIN;
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByLogin(login);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
