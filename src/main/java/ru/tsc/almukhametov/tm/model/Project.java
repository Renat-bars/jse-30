package ru.tsc.almukhametov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.entity.IWBS;
import ru.tsc.almukhametov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
public class Project extends AbstractOwnerEntity implements IWBS {


    @Nullable
    private String name = "";

    @Nullable
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @NotNull
    private Date created = new Date();

    public Project() {
    }

    public Project(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return id + ":" + name + " | " + description;
    }

}
