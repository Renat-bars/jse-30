package ru.tsc.almukhametov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    @Nullable
    E add(@Nullable final E entity);

    void addAll(@NotNull final List<E> entities);

    @Nullable
    Optional<E> remove(@Nullable final E entity);

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@Nullable Comparator<E> comparator);

    void clear();

    @Nullable
    Optional<E> findById(@Nullable final String id);

    @Nullable
    Optional<E> findByIndex(final Integer index);

    @Nullable
    Optional<E> removeById(@Nullable final String id);

    @Nullable
    Optional<E> removeByIndex(final Integer index);

    boolean existById(@Nullable final String id);

    boolean existByIndex(final int index);

    Integer getSize();

}
