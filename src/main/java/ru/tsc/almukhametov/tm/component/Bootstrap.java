package ru.tsc.almukhametov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.almukhametov.tm.api.repository.*;
import ru.tsc.almukhametov.tm.api.service.*;
import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.exception.system.UnknownArgumentException;
import ru.tsc.almukhametov.tm.exception.system.UnknownCommandException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.repository.*;
import ru.tsc.almukhametov.tm.service.*;
import ru.tsc.almukhametov.tm.util.SystemUtil;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.tsc.almukhametov.tm.enumerated.Status.COMPLETED;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final TaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthenticationRepository authenticationRepository = new AuthenticationRepository();

    @NotNull
    private final IAuthenticationService authenticationService = new AuthenticationService(userService, authenticationRepository, propertyService);

    @NotNull
    private final Backup backup = new Backup(this);

    @SneakyThrows
    public void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.almukhametov.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsc.almukhametov.tm.command.AbstractCommand.class)
                .stream()
                .sorted(Comparator.comparing(Class::getName))
                .collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            registry(clazz.getConstructor().newInstance());
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(@NotNull final String[] args) {
        System.out.println("** Welcome to THE REAL WORLD **");
        initUsers();
        initData();
        initCommands();
        process();
        backup.init();
        parseArgs(args);
        initPID();
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
        authenticationService.login("admin", "admin");

    }

    private void initData() {
        projectService.add("admin", new Project("Drum & Bass", "Music genre")).setStatus(COMPLETED);
        projectService.add("admin", new Project("Lexus", "Vehicles factory")).setStatus(Status.NOT_STARTED);
        projectService.add("admin", new Project("FAW", "Vehicles factory")).setStatus(Status.NOT_STARTED);
        projectService.add("admin", new Project("ROSATOM", "Nuclear plant")).setStatus(Status.COMPLETED);
        projectService.add("admin", new Project("Sport", "Citius, Altius, Fortius")).setStatus(Status.IN_PROGRESS);
        taskService.add("admin", new Task("Neurophunk", "podcast")).setStatus(Status.COMPLETED);
        taskService.add("admin", new Task("IS500", "Sedan cars")).setStatus(Status.NOT_STARTED);
        taskService.add("admin", new Task("NPP", "Balaklavskaya")).setStatus(Status.COMPLETED);
        taskService.add("admin", new Task("Hockey", "Tampa")).setStatus(Status.IN_PROGRESS);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);

    }

    public void parseArgs(@Nullable String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return;
        @Nullable final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public void parseArg(@Nullable final String arg) {
        if (!Optional.ofNullable(arg).isPresent()) throw new UnknownArgumentException(arg);
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownArgumentException(arg);
        command.execute();
    }

    public void parseCommands(@Nullable final String command) {
        if (!Optional.ofNullable(command).isPresent() || command.isEmpty()) return;
        @Nullable
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (!Optional.ofNullable(abstractCommand).isPresent()) throw new UnknownCommandException(command);
        @NotNull final Role[] roles = abstractCommand.roles();
        authenticationService.checkRoles(roles);
        abstractCommand.execute();
    }

    private void process() {
        logService.debug("Test environment.");
        @NotNull
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommands(command);
                logService.debug("Completed");
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }

}
