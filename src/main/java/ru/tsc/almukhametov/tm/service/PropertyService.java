package ru.tsc.almukhametov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "11";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "1";

    @NotNull
    private static final String BACKUP_INTERVAL_KEY = "interval";

    @NotNull
    private static final String BACKUP_INTERVAL_DEFAULT = "30";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }


    private String getValue(final String name, final String DefaultValue) {
        @Nullable final String systemProperty = System.getProperty(name);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getProperty(name);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(name, DefaultValue);
    }

    private int getValueInt(final String name, final String DefaultValue) {
        @Nullable final String systemProperty = System.getProperty(name);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getProperty(name);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(properties.getProperty(name, DefaultValue));
    }

    private long getValueLong(final String name, final String DefaultValue) {
        @Nullable final String systemProperty = System.getProperty(name);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getProperty(name);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Long.parseLong(properties.getProperty(name, DefaultValue));
    }


    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read("version");
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return Manifests.read("developer");
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return Manifests.read("email");
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getValueInt(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Long getBackupInterval() {
        return getValueLong(BACKUP_INTERVAL_KEY, BACKUP_INTERVAL_DEFAULT);
    }

}
